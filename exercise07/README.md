# Exercise 07 - Add rules

## Objective
In this exercise you will learn how to build logic in your form using rules and the visual rule editor.

## Add a rule
Now we will add a rule to the panel that will automatically change the text in the *Yearly amount* text box.

1. Select the **Monthly** text box.
2. Click on the **Edit Rules** button to open the *Rule Editor*. If you see a *View & Create Rules* popup you can click through it (click **Next**) or close it immediately (click **Skip**).
3. Click on **+ Create** to create a new rule.
4. In the *Yearly - When* screen, select from the *WHEN* drop-down list, **Set Value of**.
5. In the *Select Option* pick **Mathematical Expression**.
    1. In the *Drop object or select here* search for **Monthly**
    2. in the *Select Operator* pick **multiplied by**
    3. In the *Drop object or select here* click on **__<__ Form Object** and select **Number** and fill in **12**

6. Click on **Add Condition**. This will reveal the *When* area allowing you to define when the *Yearly* text box should have the value you defined at step 5.
7. Click on **Form Objects and Functions** at the top of the *Your Status - Calculate* screen. This will reveal a side pane with the *Form Objects*.
8. Drag the **Monthly** drop-down list entry from *Form Objects* and drop it on the left *Drop object or select here* underneath *When*.
9. From the *Select Operator* drop-down list, select **is not empty**.
10. Based on the above steps, add an additional condition where you use the *Income Type* component and verify whether it is equal to **Salary**.
11. Ensure the *Select Operator* in between the two conditions is set to **And**.\
Your rule editor should look like the screenshot below:\
![Rule Editor](../images/ruleeditor.png)
13. Click **Done** to close the *Your Status - Calculate* rule editor.

## Add another rule
Repeat the above steps to create an additional rule that will set the *Yearly* component to the same value as *Montly* when *Income Type* is not equal to *Salary* 
When you've done, the *Rule Editor - Yearly* should look like the screenshot below:

![Rules](../images/rules.png)

Click **Close** to close the Rule Editor. Then preview your form and see if it behaves properly and shows you the right status based on the selections you make in the *Quiz* panel.

See this Youtube [video](https://youtu.be/OLIMICi52a4) for a replay of above exercises.


## Next
* Continue to [Exercise 08](../exercise08/)
