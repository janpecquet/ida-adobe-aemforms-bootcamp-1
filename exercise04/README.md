# Exercise 04 - Add content using components

## Objective
In this exercise you will learn how to add content to the form using components and how to configure these components.

## Add components to the form
1. Click **Content** and then select the **Indentity** element in the list to ensure the **Identity** section in the form is selected.
2. Click **Component** to open the *Components* pane. You will see a list of all components you can use in your form.
3. Drag and drop a **Text Box** component on the *Identity* component in the form.
4. Select the *Text Box* and pick the **copy** option from the toolbar.
5. Select the **Identity** section and **paste** the *Text Box* component. Do this step 2 times.
4. Drag and drop an **Email** component below the *Text Box* component on the form.
5. Drag and drop a **Drop-down List** component below the *Email* component on the form.
 
## Configure the components
1. Select the first **Text Box** component and click on **Properties** in the toolbar. In the *Properties* pane:
    1. Enter **tbFirstName** as *Name*. The name of a component is important as you can use it to refer to it in rules and more advanced scripting. It is a good practice to identify your component with a somewhat descriptive name. In this lab we will use two characters to identify the type of component (e.g. `tb` for text box, `dl` for drop-down list, etc.), but you're completely free in choosing your own naming convention.
    2. Enter **First Name** as *Title*.
    3. Click on **√** to confirm.
2. Select the second **Text Box** component and click on **Properties** in the toolbar. In the *Properties* pane:
    1. Enter **tbLastName** as *Name*.
    2. Enter **Last Name** as *Title*.
    3. Click on **√** to confirm.
3. Select the third **Text Box** component and click on **Properties** in the toolbar. In the *Properties* pane:
    1. Enter **tbMobilePhone** as *Name*.
    2. Enter **Mobile Phone** as *Title*.
    3. Click on **√** to confirm.
4. Select the **Email** component and click on **Properties** in the toolbar. In the *Properties* pane:
    1. Enter **ebEmail** as *Name*.
    2. Scroll down and click on **Patterns**. You will see this email component has a specific validation pattern defined (using a regular expression `^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$`) to ensure a valid email is provided. Don't worry, you do not have to remember this. The email component is nothing else than a more sophisticated text box component with this additional validation already applied for you.
    3. Click on **√** to confirm.
5. Select the **Drop-down List** component and click on **Properties** in the toolbar. In the *Properties* pane:
    1. Enter **dlGender** as *Name*.
    2. Enter **Gender** as *Title*.
    3. Click on **Add** in the *Items* section of the *Properties* pane and in the text box that appears, type `Male`. 
    4. Repeat this to add more countries, e.g. `Female`.
    5. Enter `Select Gender` in the *Placeholder Text* field.
    6. Click on **√** to confirm.
   
Your form should now look like as in the screenshot below:

![Identity section](../images/people.png)

## Modify the layout
As you can see, all the components take the whole width of the form. If we want to position them horizontally next to each other we can do so by positioning them in a (sub-)panel. To do so:

1. Go back to the *Components* pane by clicking on **Components**.
2. Drag a **Panel** component and drop it in between the *Gender* and *Email* component on the form.
3. Switch to the *Content* pane by clicking on **Content**.
4. Drag **First Name** component in the *Content* pane and drop it in that same pane on top of the **Section** component.
5. Drag **Last Name** component in the *Content* pane and drop it in that same pane on top of the **Section** component.
4. Drag **Gender** component in the *Content* pane and drop it in that same pane on top of the **Section** component.
5. Drag **Mobile Phone** component in the *Content* pane and drop it in that same pane on top of the **Section** component.
5. Drag **Email** component in the *Content* pane and drop it in that same pane on top of the **Section** component.
6. Select **Section** entry in the *Content* pane and while hovering, click on the **Configure** (wrench) icon on the right of that selected entry. This will open the *Properties* pane.
7. In the *Properties* pane, select **2** in the *Number of Columns* dropdown listbox.
8. Click on **√** to confirm changes.
9. Select *Email* component in the *Content* pane and click on the **Configure** button in the toolbar.
10. In the *Properties* pane, select **2** from the *Colspan* dropdown listbox.

*First Name*, *Last Name*, *Gender* and *Mobile Phone* will now be positioned as a 2 by 2 grid. However, when you make your browser window smaller, you will see that the form will behave responsive and position the fields underneath of each other. *Email* on the other hand will be displayed across the whole width of the panel, despite that the panel is configured to consist of two columns.

Your form should now look like as in the screenshot below:

![Section](../images/section.png)


## Next
* Continue to [Exercise 05](../exercise05/)
